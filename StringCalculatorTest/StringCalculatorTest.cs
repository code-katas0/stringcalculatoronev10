﻿using NUnit.Framework;
using StringCalculatorV10;
using System;

namespace StringCalculatorTest
{    
    [TestFixture]
    public class StringCalculatorTest
    {
        private StringCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
            _calculator = new StringCalculator();
        }

        [Test]
        public void GivenEmptyString_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 0;

            //Act
            var actual = _calculator.Add("");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenOneNumber_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 1;

            //Act
            var actual = _calculator.Add("1");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenTwoNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("1,2");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenUnlimitedNumbers_WhenAdding_ThenReturnsum()
        {
            //Arrange
            var expected = 5;

            //Act
            var actual = _calculator.Add("1,2,2");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenNewLineAsDelimiter_WhenAdding_ThenRetutrnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("1\n2,3");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenCustomDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("//;\n1;2");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenNegativeNumbers_WhenAdding_ThenThrowExpction()
        {
            var expected = "Negatives not Allowed. -3 -4";

            //Act
            var exception = Assert.Throws<Exception>(() => _calculator.Add("//;\n2;-3;-4"));

            //Assert
            Assert.AreEqual(expected, exception.Message);
        }

        [Test]
        public void GivenNumbers_WhenAdding_ThenIgnoreNumbersGreaterThousand()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("//;\n2;1001;1");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenAnyLengthOfDelemiliters_When_Adding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("//***\n1***2***3");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenMultipleDelimiters_WhenAdding_ThenReturn_Sum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("//[*][%]\n1*2%3");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenInvalidDelimiter_WhenAdding_ThenThrowException()
        {
            //Arrange
            var expected = "Invalid Delimiter !.";

            //Act
            var exception = Assert.Throws<Exception>(() => _calculator.Add("//[*][%]\n1*2$3"));

            //Assert
            Assert.AreEqual(expected, exception.Message);
        }

        [Test]
        public void GivenUnlimitedMultipleDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("//[$$*][##%&]\n1$$*2##%&3");

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
