﻿using System.Collections.Generic;
using System;

namespace StringCalculatorV10
{
    public class StringCalculator
    {
        private char comma = ',';
        private char slash = '/';
        private char newLine = '\n';
        private char leftSquare = '[';
        private char rightSquare = ']';
        private string doubleSlash = "//";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            var delimiters = GetDelimiters(numbers);
            var numberList = GetNumberList(numbers, delimiters);
            var total = GetTotal(numberList);
            
            return total;
        }

        private List<string>GetDelimiters(string numbers)
        {
            var delimiters = new List<string>(new string[] { comma.ToString(), newLine.ToString() });

            if (numbers.Contains(doubleSlash))
            {
                numbers = string.Concat(numbers.Split(slash));

                var delimiter = numbers.Substring(0,numbers.IndexOf(newLine));

                delimiters.Clear();
                delimiters.Add(delimiter);

                if (numbers.Contains(leftSquare.ToString()))
                {
                    var multipleDelimiters = numbers.Substring(numbers.IndexOf(leftSquare), numbers.LastIndexOf(rightSquare));
                    
                    delimiters.Clear();

                    foreach (var customDelimiter in multipleDelimiters.Split(leftSquare, rightSquare))
                    {
                        delimiters.Add(customDelimiter);
                    }
                }
            }
            
            return delimiters;
        }
        private List<int> GetNumberList(string numbers, List<string> delimiterlist)
        {
            var numberList = new List<int>();
            var negativeNumbers = string.Empty;

            numbers = string.Concat(numbers.Split(slash,leftSquare,rightSquare));

            foreach (var number in numbers.Split(delimiterlist.ToArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    var convertedNumber = int.Parse(number);

                    if (convertedNumber < 0)
                    {
                        negativeNumbers = string.Join(" ", negativeNumbers, number);
                    }

                    numberList.Add(convertedNumber);
                }

                catch
                {
                    throw new Exception("Invalid Delimiter !.");
                }
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                throw new Exception("Negatives not Allowed." + negativeNumbers);
            }

            return numberList;
        }

        private int GetTotal(List<int> numberList)
        {
            var sum = 0;

            foreach (var number in numberList)
            {
                if (number <= 100)
                {
                    sum += number;
                }
            }

            return sum;
        }
    }
}
